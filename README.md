Les contributeurs de ce document sont : Bertrand Vernay, Nicolas Goudin, Gabriel Le Goff, Marie-Claire Blache, Alexandre Hego, Laetitia Besse, Héloïse Monnet, Cédric Messaoudi, Lene Vimeux, Aurélien Maillot, Lhorane Lobjois.

1. [Prérequis](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Pr%C3%A9requis) 
   * [Glossaire](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Pr%C3%A9requis#glossaire-mcb)
   * [Annotations d'un jeu de données](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Pr%C3%A9requis#annotations-dun-jeu-de-donn%C3%A9es-gabriel)
   * [Solution en ligne](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Pr%C3%A9requis#solution-en-ligne-c%C3%A9dric)
2. [Installation Cellpose sur Windows 10](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Cellpose-sur-Windows10) 
   * [Installation logiciels sous Windows](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Cellpose-sur-Windows10#installation-logiciels-sous-windows)
     * FIJI ou ImageJ
     * Qupath
   * [Installation GPU - non nécessaire mais recommandé](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Cellpose-sur-Windows10#installation-gpu-non-n%C3%A9cessaire-mais-recommand%C3%A9-alex)
   * [Installation Cellpose via Anaconda3](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Cellpose-sur-Windows10#installation-cellpose-via-anaconda3-alex-nicolas)
     * Téléchargement d'Anaconda3
     * Installation d'Anaconda3
     * Utilisation d'Anaconda3
     * Installer Cellpose2 dans un environnement dédié
     * Cellpose avec la GPU 
     * Installation scikit-image pour QuPath
     * Exécution de cellpose2
   * [Test de l'installation de Cellpose sur Anaconda](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Cellpose-sur-Windows10#test-de-linstallation-de-cellpose-sur-anaconda)
   * [Bridge Cellpose2 – Fiji / QuPath](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Cellpose-sur-Windows10#bridge-cellpose2-fiji-qupath)
     * Installation de cellpose2
     * Autoriser Windows à utiliser vos environnements anaconda3
       * Modification des variables d’environnement système
       * Initialisation de conda par Windows
       * Vérifier si ça a fonctionné
     * Installation du plugin cellpose2 sous FIJI
       * Installation
       * Paramétrage et exécution du plugin
       * Utilisation de Cellpose2 sous FIJI
     * Installation de l'extension cellpose2 sous Qupath
       * Installation
       * Paramétrage et exécution de l'extension
       * Utilisation de Cellpose2 sous QuPath
3. [Installation Cellpose sur linux](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Installation-Linux)
   * Installation GPU - non nécessaire mais recommandé
   * Installation de Minoconda
   * Création d'un environnement Cellpose 2 avec Miniconda
     * Cellpose sans GPU
     * Cellpose avec la GPU
   * Exécution de cellpose avec la GUI
   * Bridge Cellpose2 – FIJI
     * Installation de FIJI
     * Installation du plugin cellpose
     * Configuration du plugin Cellpose
     * Vérifier l'installation Bridge Cellpose2 – FIJI
   * Bridge Cellpose2 – Qupath
     * Installation Qupath
      * Installation des dépendances
      * Compilation de la dernier version quath v4.4.0
     * Installation de Cellpose 2 sur Qupath V4.0.0
     * Vérifier l'installation Bridge Cellpose2 – Qupath
4. [Utilisation des models Cellpose déjà entrainé](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Utilisation-des-models-Cellpose-d%C3%A9j%C3%A0-entrain%C3%A9)
   * [GUI](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Utilisation-des-models-Cellpose-d%C3%A9j%C3%A0-entrain%C3%A9#gui-alex) 
   * [QuPath](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Utilisation-des-models-Cellpose-d%C3%A9j%C3%A0-entrain%C3%A9#qupath-nicolas-alex)
   * [FIJI](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Utilisation-des-models-Cellpose-d%C3%A9j%C3%A0-entrain%C3%A9#fiji-laetitia-alex)
5. [Contrôle qualité et métriques](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Contr%C3%B4le-qualit%C3%A9-et-m%C3%A9triques) 
   * [Contrôle qualité](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Contr%C3%B4le-qualit%C3%A9-et-m%C3%A9triques#contr%C3%B4le-qualit%C3%A9)
     * Méthodologie
     * Analyse des métriques
       * Au niveau pixel
       * Au niveau objet
   * [FIJI](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Contr%C3%B4le-qualit%C3%A9-et-m%C3%A9triques#fiji-laetitia-et-c%C3%A9dric)
     * Comparaison de masques sous ImageJ/Fiji
     * Installation du plugin Mask instant Comparator (MiC)
     * Lancer le plugin Mask instant Comparator
   * [Qupath](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Contr%C3%B4le-qualit%C3%A9-et-m%C3%A9triques#qupath-nicolas)
6. [Faire son propre model Cellpose - Entrainement](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Faire-son-propre-model-Cellpose-Entrainement)
   * [Quand ré-entrainer un modèle?](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Faire-son-propre-model-Cellpose-Entrainement#quand-r%C3%A9-entrainer-un-mod%C3%A8le)
   * [GUI](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Faire-son-propre-model-Cellpose-Entrainement#gui-alex)
     * Faire les annotations 
     * Entrainement
     * Prédictions
   * [Qupath](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Faire-son-propre-model-Cellpose-Entrainement#qupath-nicolas)
     * Prérequis
     * Créer votre projet et votre jeu de données
     * Créer vos cadres Training et Validation
     * Créer vos annotations
     * Entrainement
     * Contrôle qualité

7. [Bonnes pratiques de publication](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Bonnes-pratiques-de-publication) 

8. [Exemple : étude de cas](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/Exemple-%C3%A9tude-de-cas)
   
9.  [Bibliographie, informations complémentaires et sources](https://forgemia.inra.fr/gt-maiia/kit-du-debutant/-/wikis/bibliographie-informations-compl%C3%A9mentaires-et-sources)
